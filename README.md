# 审计工具箱 Audbox 

> @微信公众号：效率视界
> 
> @作者：王文铖

## 📢懒人目录（点击跳转）

[一、工具界面设计](https://gitee.com/wwwwwc/audbox/blob/master/README.md#一工具界面设计)

[二、工具详细视频教程（一定要看🌵🌵🌵）](https://gitee.com/wwwwwc/audbox/blob/master/README.md#二工具详细视频教程)

[三、对工具有疑问？在这里提交问题回复最快！](https://gitee.com/wwwwwc/audbox/blob/master/README.md#三对工具有疑问在这里提交问题回复最快)

[四、记得给我“标星”⭐⭐⭐](https://gitee.com/wwwwwc/audbox/blob/master/README.md#四记得给我标星)

[五、下载审计工具箱](https://gitee.com/wwwwwc/audbox/blob/master/README.md#五下载审计工具箱)

---
---

## 🎴一、工具界面设计


<img src="https://xlsj.oss-cn-beijing.aliyuncs.com/20210129004551.png" width="80%"/>


## 📚二、工具详细视频教程

<a href="https://mp.weixin.qq.com/s/KjHwxPweZydKS9SWdISRow" target="_blank"> 0. **安装火狐浏览器**  </a>

<a href="https://mp.weixin.qq.com/s/KjHwxPweZydKS9SWdISRow" target="_blank"> 1. **快递截图查询** </a>

<a href="https://mp.weixin.qq.com/s/CuDrGX1hu88XT9bK4WDR4w" target="_blank"> 2. **工商截图查询** </a>

<a href="https://mp.weixin.qq.com/s/YvD6b1_07t79P0lCgHDkbA" target="_blank"> 3. **地址复核截图** </a>

<a href="https://mp.weixin.qq.com/s/PL2GcVjsI6FfQnCbcg8Fsw" target="_blank"> 4. **询证函表格批量斜线转无、无转斜线** </a>


## ❓三、对工具有疑问？在这里提交问题回复最快！

<a href="https://gitee.com/wwwwwc/audbox/issues" target="_blank"> ☞**点击这里提交问题** </a>，这样我手机上会第一时间收到并予以回复。

<img src="https://xlsj.oss-cn-beijing.aliyuncs.com/20210123115311.png" width="80%"/>


## ⭐四、记得给我“标星”

<img src="https://xlsj.oss-cn-beijing.aliyuncs.com/20210119212810.png" width="80%"/>

## 📦五、下载审计工具箱

<a href="https://gitee.com/wwwwwc/audbox/releases" target="_blank"> ☞**点击这里下载工具** </a>

下载最新版本的zip压缩包即可，不要直接用微信打开链接直接下载，最好用浏览器或者IDM、NDM等下载器下完完整安装包，如果下载较慢，可以尝试切换网络，部分公司网络对Gitee有限制。

![](https://xlsj.oss-cn-beijing.aliyuncs.com/20210128143243.png)

